# Generated by Django 3.1.4 on 2021-02-04 19:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pedido', '0003_auto_20210204_1632'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pedido',
            old_name='user_cli',
            new_name='user',
        ),
    ]
