# Generated by Django 3.1.4 on 2021-02-04 20:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pedido', '0005_auto_20210204_1732'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pedido',
            old_name='userr',
            new_name='user',
        ),
    ]
