# Generated by Django 3.1.4 on 2021-02-04 19:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pedido', '0002_auto_20210204_1434'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pedido',
            old_name='user',
            new_name='user_cli',
        ),
    ]
