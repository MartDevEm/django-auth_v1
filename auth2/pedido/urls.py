from django.urls import path
from . import views

urlpatterns = [
        path('pedido-viewset/', views.Pedido_ViewSet.as_view(), name='pedido-viewset'),
        path('pedido-setdetail/<int:pk>', views.Pedido_SetDetail.as_view(), name='pedido-setdetail'),
        path('register/', views.RegisterView.as_view(), name='register')
        ]
