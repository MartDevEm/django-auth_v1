from .models import Pedido
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password


class Pedido_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Pedido
        fields = ('id','descripcion')


class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
            required = True,
            validators = [UniqueValidator(queryset=User.objects.all())]
            )

    #password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    #password2 = serializers.CharField(required=True, validators=[validate_password])

    class Meta:
        model = User
        fields = ('username','password','email','first_name','last_name')
        extra_kwargs = {
                'first_name':{'required':True},
                'last_name': {'required':True},
                'password':{'write_only':True}
                 #'password2':{'required':True}
                }

        def validate(self, attrs):
            if attrs['password'] != attrs['password2']:
                raise serializers.ValidationError({"password":"Password fields did not match!"})

            return attrs

        def create(self, validated_data):
            user = User.objects.create(
                    username = validated_data['username'],
                    email = validated_data['email'],
                    first_name = validated_data['first_name'],
                    last_name = validated_data['last_name'],
                    password = make_password('password')

                    )

            #user.set_unusable_password()#validated_data['password'])
            #user.set_password(validated_data['password'])
            user.save()

            return user
