from django.shortcuts import render
from .models import Pedido
from .serializers import Pedido_Serializer, RegisterSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.contrib.auth.hashers import make_password

User = get_user_model()

class Pedido_ViewSet(generics.ListCreateAPIView):
   
    print("Hellooooo")
    queryset = Pedido.objects.all()
    permission_clsses = (IsAuthenticated,)
    serializer_class = Pedido_Serializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return Pedido.objects.filter(user=self.request.user)
    
class Pedido_SetDetail(generics.RetrieveUpdateDestroyAPIView):
    
    queryset = Pedido.objects.all()
    serializer_class = Pedido_Serializer


class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer

    def perform_create(self, serializer):
        password = make_password(self.request.data['password'])

        serializer.save(password=password)



